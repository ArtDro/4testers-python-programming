# Exercise 1
def get_number_squared(num):
    return num ** 2


zero_squared = get_number_squared(0)
print(zero_squared)

sixteen_squared = get_number_squared(16)
print(sixteen_squared)

floating_number_squared = get_number_squared(2.55)
print(floating_number_squared)


# My Exercise

def square(number):
    return number ** 2


number1 = 0
number2 = 16
number3 = 2.55

print(square(number1))
print(square(number2))
print(square(number3))
print(square(4))


# Exercise 2
def cuboid_volume(a, b, c):
    return a * b * c


print(cuboid_volume(3, 5, 7))


# My Exercise
# volume of a cuboid
# V = a * b * c

def volume_of_cuboid(a, b, c):
    return a * b * c


print(volume_of_cuboid(3, 5, 7))


# Exercise 3
def convert_temperature_in_celsius_to_fahrenheit(temp_in_celsius):
    return temp_in_celsius * 9 / 5 + 32


fahrenheit = convert_temperature_in_celsius_to_fahrenheit(20)
print(fahrenheit)

# My exercise

# conversion of degree
# F = (C * 9/5) + 32
# C = (F-32) * 5/9

print("converting 20 Celsius degrees to Fahrenheit")


def conversion_of_degree(fahrenheit):
    return (fahrenheit * 9 / 5) + 32


fahrenheit = conversion_of_degree(20)
print(fahrenheit)

print("Converting Celsius degrees to Fahrenheit and vice versa ")
option = input("What you want to do? C -> F press 1, F -> C press 2.")

if option == '1':
    print('Exchange C -> F')
    value = int(input("Enter the value in Celsius: "))
    result = (value * 9 / 5) + 32
    print(f'{value} C = {result} F')
elif option == '2':
    print('Exchange F -> C')
    value = int(input('Enter the value in Farenheit: '))
    result = (value - 32) * 5 / 9
    print(f' {value} F = {result} C')
else:
    print("I don't know what do you want to do?")
    quit()

print('The End!')
