import random
import string


def get_list_element_sum(list_with_numbers):
    return sum(list_with_numbers)


temperatures_in_january = [-4, 1.0, -7, 2]
temperatures_in_february = [-13, -9, -3, 3]

print(f'January: {get_list_element_sum(temperatures_in_january)}')
print(f'February: {get_list_element_sum(temperatures_in_february)}')


def get_two_numbers_and_returned_their_average(first_number, second_number):
    return (first_number + second_number) / 2


first_number = 5
second_number = 5
print(get_two_numbers_and_returned_their_average(first_number, second_number))


# Exercise 1

def print_hello_message(name, city):
    print(f"Witaj {name.capitalize()}! Miło Cię widzieć w naszym mieście: {city.capitalize()}!")


print_hello_message("Michał", "Toruń")
print_hello_message("Beata", "Gdynia")
print_hello_message("artur", "opole")


# Exercise 2

def get_email_4testers(first_name, last_name):
    return f"{first_name.lower()}.{last_name.lower()}@4testers.pl"


print(get_email_4testers("Janusz", "Nowak"))
print(get_email_4testers("Barbara", "Kowalska"))

# # Exercise 3
# # Create a function description player
#
# def get_description_of_player(player_dictionary):
#     player_name = player_dictionary["nick"]
#     player_type = player_dictionary["type"]
#     player_exp = player_dictionary["exp-points"]
#     return f'The player {player_name} is of type {player_type} and has {player_exp} EXP'
#
#
# player_one = {
#     "nick": "maestro_54",
#     "type": "warrior",
#     "exp-points": 3000
# }
# player_two = {
#     "nick": "artys",
#     "type": "mag",
#     "exp-points": 12000
# }
#
# print(get_description_of_player(player_one))
# print(get_description_of_player(player_two))
#
#
# # Exercise 4
# # Create a function that generates random log data
#
#
# def get_random_password(password_len):
#     random_source = string.ascii_lowercase + string.digits
#     password = random.choice(string.ascii_lowercase)
#     password += random.choice(string.digits)
#     for i in range(password_len):
#         password += random.choice(random_source)
#
#     password_list = list(password)
#     random.SystemRandom().shuffle(password_list)
#     password = ''.join(password_list)
#     return password
#
#
# def get_random_login_of_data(user_email):
#     return {
#         "email": user_email,
#         "password": get_random_password(7)
#     }
#
#
# user_email = "user@example.com"
#
# print(get_random_login_of_data(user_email))

if __name__ == '__main__':
    adresses = [
        {"city": "Opole", "street": "Zielona", "house_number": 1, "post_code": "45-401"},
        {"city": "Wrocław", "street": "Czerwona", "house_number": 4, "post_code": "70-701"},
        {"city": "Kraków", "street": "Zółta", "house_number": 15, "post_code": "35-100"},
        {"city": "Warszawa", "street": "Brązowa", "house_number": 7, "post_code": "12-112"}
    ]

    print(adresses[-1]["post_code"])
    print(adresses[1]["city"])

    adresses[0]["street"] = "Różowa"
    print(adresses)


# exercise 5 - if, else

def check_if_normal_conditions(celsius, pressure):
    if celsius == 0 and pressure == 1013:
        return True
    else:
        return False


print(check_if_normal_conditions(0, 1013))
print(check_if_normal_conditions(1, 1014))
print(check_if_normal_conditions(0, 1014))
print(check_if_normal_conditions(1, 1014))

# exersice

import uuid


def get_random_login_of_data(user_email):
    password = uuid.uuid4()
    return {
        "email": user_email,
        "password": f"{password}"
    }


print(get_random_login_of_data("fe@sfa.pl"))
print(get_random_login_of_data("a@example.com"))
print(get_random_login_of_data("b@example.com"))
print(get_random_login_of_data("cvx@example.com"))
print(get_random_login_of_data("xcvx@example.com"))

# Exercise 2 instrukcja While
n = 5
while n > 0:
    print(n)
    n = n - 1
print('Start !')




