# Exercise list of dictionaries

childrens = [

    {
        "age": 7,
        "growth": 124,
        "name": "Iga",
        "male": False

    },

    {
        "age": 9,
        "growth": 132,
        "name": "Emilia",
        "male": False

    },

    {
        "age": 2,
        "growth": 98,
        "name": "Pola",
        "male": False

    }
]

print(len(childrens))
print(childrens[0])
print(childrens[-1])
print(childrens[0]["name"])
print(childrens[1]["growth"])

childrens.append(
    {
        "age": 12,
        "growth": 128,
        "name": "Artur",
        "male": True
    }
)

print(len(childrens))
print(childrens)


# Conditional statements  - struktury warunkowe

def print_temperature_description(temperature_in_celsius):
    print(f"Temperature right now is {temperature_in_celsius} Celsius degrees")
    if temperature_in_celsius > 25:
        print("It's getting hot!")
    elif temperature_in_celsius >= 0:
        print("It's quite OK")
    else:
        print("It's getting cold!")


def is_person_an_adult(age):
    if age >= 18:
        return True
    else:
        return False
    # return True if age >= 18 else False


# Exersice conditional statements

def are_you_older_then_Artur(your_age):
    if your_age > 40:
        print("Artur is 40 years old, you are oldest than Artur")
    elif your_age == 40:
        print("You are the same age like Artur")
    else:
        print("Artur is 40 years old, you are younger then Artur")


if __name__ == '__main__':
    temperature_in_celsius = 0
    print_temperature_description(temperature_in_celsius)
    age_emi = 9
    age_tom = 18
    age_marta = 21
    age_Artur = 28
    are_you_older_then_Artur(age_Artur)
    print("Emi", is_person_an_adult(age_emi))
    print("Tom", is_person_an_adult(age_tom))
    print("Marta", is_person_an_adult(age_marta))


