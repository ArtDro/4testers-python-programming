from cars import get_country_of_a_car_brand, get_gas_usage_for_distance


def test_get_country_for_a_japanese_car():
    assert get_country_of_a_car_brand('Toyota') == 'Japan'


def test_get_country_for_a_japanese_car_lower():
    assert get_country_of_a_car_brand('toyota') == 'Japan'


def test_get_country_for_a_german_car():
    assert get_country_of_a_car_brand('Mercedes') == 'Germany'


def test_get_country_for_a_german_car_lower():
    assert get_country_of_a_car_brand('mercedes') == 'Germany'


def test_get_country_for_a_french_car():
    assert get_country_of_a_car_brand('Renault') == 'France'


def test_get_country_for_a_french_car_lower():
    assert get_country_of_a_car_brand('renault') == 'France'


def test_get_country_for_a_italian_car():
    assert get_country_of_a_car_brand('Fiat') == 'Unknown'


# Tests for a function calculating gas usage

def test_gas_usage_for_zero_distance_driven():
    assert get_gas_usage_for_distance(0, 8.5) == 0

