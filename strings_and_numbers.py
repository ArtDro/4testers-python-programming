first_name = "Artur"
last_name = "Drozd"
email = "drozd.artur@gmail.com"

print("Mam na imię", first_name + ".", "Moje nazwisko to", last_name + ".")

my_bio = "Mam na imię " + first_name + ". Moje nazwisko to " + last_name + ". Mój email to : " + email + "."
print(my_bio)

# F-string
my_bio_using_f_string = f"Mam na imie {first_name}. \nMoje nazwisko to {last_name}. \nMój email to: {email}."
print(my_bio_using_f_string)

print(f"Wynik operacji mnożenia 4 przez 5 to {4 * 5}")

## Algebra
circle_radius = 4
area_of_a_circle_with_radius_5 = 3.14 * circle_radius ** 2
circumference_of_a_circle_with_radius_5 = 2 * 3.14 * circle_radius

print(f"Area of the circle with radius {circle_radius}:", area_of_a_circle_with_radius_5)
print(f"Circumference of a circle with radius {circle_radius}:", circumference_of_a_circle_with_radius_5)

long_mathematical_expression = 2 + 3 + 5 * 7 + 4 / 3 * (3 + 5 / 2) + 7 ** 2 - 13
print(long_mathematical_expression)


my_name = "Adrian"
favourite_movie = "Star Wars"
favourite_actor = "Clint Eastwood"

print("My name is ", my_name, ". My favourite movie is ", favourite_movie, ". My favourite actor is ", favourite_actor,
      ".", sep="")

bio = f"My name is {my_name}. My favourite movie is {favourite_movie}. My favourite actor is {favourite_actor}."
print(bio)

calculation_example = f"Sum of 2 and 4 is {2 + 4}."
print(calculation_example)


# Exercise 1 - Print welcome message to a person in a city
def print_hello_message(name, city):
    name_capitalized = name.capitalize()
    city_capitalized = city.capitalize()
    # print(f"Witaj {name.capitalize()}! Miło Cię widzieć w naszym mieście: {city.capitalize()}!")
    print(f"Witaj {name_capitalized}! Miło Cię widzieć w naszym mieście: {city_capitalized}!")


print_hello_message("Michał", "Toruń")
print_hello_message("Beata", "Gdynia")
print_hello_message("adam", "kraków")


# Exercise 2 - Generate email in 4testers.pl domain using first and last name
def get_email(name, surname):
    return f'{name.lower()}.{surname.lower()}@4testers.pl'


print(get_email("Janusz", "Nowak"))
print(get_email("Barbara", "Kowalska"))

# Escaping characters
print('McDonald\'s')
print("My favourite book is \"Steppenwolf\"")
print('My favourite book is "Steppenwolf"')




