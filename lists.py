movies = ['Zielone rękawiczki', 'Stranger things', 'Terminator', 'Manifest', 'Locke & Key']

first_movies = movies[0]
print(f'First move is {first_movies}')

emails = ['a@example.com', 'b@example.com']

print(len(emails))
print(emails[0])
print(emails[-1])
emails.append('cde@example.com')

print(emails)
print(len(emails))

