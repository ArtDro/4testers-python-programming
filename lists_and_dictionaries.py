shopping_list = ['oranges', 'water', 'chicken', 'potatoes', 'washing liquid']
print(shopping_list[0])
print(shopping_list[2])
print(shopping_list[-1])
print(shopping_list[-2])
shopping_list.append('lemon')
print(shopping_list)
print(shopping_list[-1])

number_of_items_to_buy = len(shopping_list)
print(number_of_items_to_buy)

first_three_shopping_items = shopping_list[0:3]
print(first_three_shopping_items)

animal = {
    "name": "Burek",
    "kind": "dog",
    "age": 7,
    "male": True
}
dog_age = animal["age"]
print("Dog age:", dog_age)
dog_name = animal["name"]
print("Dog name:", dog_name)

animal["age"] = 10
print(animal)
animal["owner"] = "Staszek"
print(animal)

# Exercise dictionary

friends = {
    'name': 'Jola',
    'age': 36,
    'hobbies': ['golf', 'surfing']
}
print(friends['age'])
friends['city'] = 'Opole'
print(friends)


def get_employee_info(email):
    return {
        'employee_email': email,
        'company': '4testers.pl'
    }


if __name__ == '__main__':
    info_1 = get_employee_info('robert@4testers.pl')
    info_2 = get_employee_info('magda@4testers.pl')
    info_3 = get_employee_info('zosia@4testers.pl')

    print(info_1)
    print(info_2)
    print(info_3)


