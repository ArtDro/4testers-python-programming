import uuid


def print_random_uuids(number_of_strings):
    for i in range(number_of_strings):
        print(str(uuid.uuid4()))


def print_numbers_multiplied_by_4(from_number, to_number, step):
    for number in range(from_number, to_number + 1, step):
        print(number * 4)


def print_numbers_divisible_by_7(from_number, to_number):
    for number in range(from_number, to_number + 1):
        if number % 7 == 0:
            print(number)


def print_temperatures_in_both_scales(list_of_temps_in_celsius):
    for temp in list_of_temps_in_celsius:
        temp_fahrenheit = temp * 9 / 5 + 32
        print(f'Celsius: {temp}, Fahrenheit: {temp_fahrenheit}')


def get_temperatures_higher_than_20_degrees(list_of_temps_in_celsius):
    filtered_temperatures = []
    for temp in list_of_temps_in_celsius:
        if temp > 20:
            filtered_temperatures.append(temp)
    return filtered_temperatures


def print_each_student_name_capitalized(list_of_student_first_name):
    for first_name in list_of_student_first_name:
        print(first_name.capitalize())


def print_first_ten_intigers_squared():
    # first_ten_integers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    # for integer in first_ten_integers: lub
    for integer in range(1, 11):
        print(integer ** 2)


if __name__ == '__main__':
    print_random_uuids(2)
    print_numbers_multiplied_by_4(0, 100, 20)
    print_numbers_divisible_by_7(1, 30)

    temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    print_temperatures_in_both_scales(temps_celsius)

    print(get_temperatures_higher_than_20_degrees(temps_celsius))

    list_of_students = ["artur", "emilia", "michAlina", "iga", "POLA"]
    print_each_student_name_capitalized(list_of_students)
    print_first_ten_intigers_squared()

# Exercise

for i in range(0, 11, 2):
    print(i)

text = "Artur uczy się pythona"
for letter in text:
    print(letter)

lista = [10, "jakiś tekst", 24.5, "hej"]
for i in lista:
    print(i)

w = 0
while w == 0:
    w = int(input("Podaj liczbę większą od zera: "))
    if w <= 0:
        print("Hej! Miała być większa od zera!")
else:
    print("Twój szczęśliwy numerek to {number}.\n\nKoniec programu!".format(number=w ** 2))
