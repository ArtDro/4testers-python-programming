import random


def generate_random_male_and_female_name():
    random_male_names_and_female_names = random.choice(female_fnames + male_fnames)
    return random_male_names_and_female_names


def generate_random_country(list_of_countries):
    random_country = random.choice(list_of_countries)
    return random_country


def generate_random_surname(list_of_surnames):
    random_surname = random.choice(list_of_surnames)
    return random_surname


def generate_random_email(first_name, last_name):
    return f'{first_name.lower()}.{last_name.lower()}@example.com'


def generate_random_age():
    age = random.randint(5, 45)
    return age


def age_upper_then_18(person_age):
    if person_age >= 18:
        return True
    else:
        return False


def birth_year(current_year, person_age):
    person_year_of_birth = current_year - person_age
    return person_year_of_birth


def get_dictionary_with_random_personal_data():
    random_first_name = generate_random_male_and_female_name()
    random_surname = generate_random_surname(surnames)
    random_country = generate_random_country(countries)
    random_email = generate_random_email(random_first_name, random_surname)
    random_age = generate_random_age()
    check_person_age = age_upper_then_18(random_age)
    check_person_year_of_birth = birth_year(2023, random_age)
    return [
        {
            'firstname': random_first_name,
            'lastname': random_surname,
            'country': random_country,
            'email': random_email,
            'age': random_age,
            'adult': check_person_age,
            'birth_year': check_person_year_of_birth
        }

    ]


if __name__ == '__main__':
    # get random both names
    female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
    male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
    generate_random_male_and_female_name()

    # get random country
    countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']
    generate_random_country(countries)

    # get random surname
    surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
    generate_random_surname(surnames)

    # get random email
    generate_random_email(generate_random_male_and_female_name(), generate_random_surname(surnames))

    # get random age
    generate_random_age()

    # age upper then 18
    age_upper_then_18(generate_random_age())

    # birth year
    birth_year(2023, generate_random_age())

    # personal data list
    print(get_dictionary_with_random_personal_data())

    # final_list
    list_of_persons = []
    for person in range(10):
        list_of_persons.append(get_dictionary_with_random_personal_data())
    print(list_of_persons)

    # self introduction
