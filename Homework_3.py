# Exercise 1
def get_names_starting_if_some_letter(friends_names):
    for name in friends_names:
        if name.count("M"):
            print("Hello", name, ", nice to meet you!")
        elif name.startswith("A"):
            print("Hello", name, ", nice to meet you!")


# Exercise 2 - Print a number from 1 to 30 divisible by 7
def print_numbers_divisible_by_7(from_number, to_number):
    for number in range(from_number, to_number + 1):
        if number % 7 == 0:
            print(number)


# Exercise 3 - get a numbers which finds numbers divisible by another number in the range from-to integers
def find_divisible_subset(number_from, number_to, divisible_by):
    divisible_subset = []
    for number in range(number_from, number_to + 1):
        if number % divisible_by == 0:
            divisible_subset.append(number)
    return f'Numbers divisible by {divisible_by} from range {number_from, number_to} are: {divisible_subset}'


# Exercise 4 - Create a list of temperatures (in Celsius degrees) and print each with temperature in Celsius and
# Fahrenheit.
def print_temperatures_in_both_scales(list_of_temps_in_celsius):
    for temp in list_of_temps_in_celsius:
        temp_fahrenheit = temp * 9 / 5 + 32
        print(f'Celsius: {temp}, Fahrenheit: {round(temp_fahrenheit, 2)}')


# Exercise 5 - Create a list of temperatures (in Celsius degrees) and return a list with Fahrenheit
def print_temperatures_a_list_in_fahrenheit(list_of_temps_celsius):
    list_of_temps_fahrenheit = []
    for temperature in list_of_temps_celsius:
        list_of_temps_fahrenheit.append(temperature * 9 / 5 + 32)
    return f'The list of temperatures in Fahrenheit : {list_of_temps_fahrenheit}'


if __name__ == '__main__':
    # Exercise 1
    friend_names = ["Adrianna", "Alicja", "Artur", "Adrian", "Basia", "Bartek", "Borys", "Cezary", "Celina", "Darek",
                    "Dagmara", "Dawid", "Ela", "Edward", "Emil", "Emilia", "Franek", "Fatima", "Grzegorz", "Gabriela",
                    "Henryk", "Iza", "Jola", "Igor", "Jarek", "Kasia", "Karol", "Kornel", "Lucjan", "Lara", "Łucja",
                    "Marta", "Marek", "Michalina", "Natalia", "Natan", "Norbert", "Olek", "Ola", "Patryk", "Paulina",
                    "Piotr", "Paweł", "Robert", "Radek", "Renata", "Roma", "Staszek", "Sylwester", "Tosia", "Tymek",
                    "Waldek", "Weronika", "Zenek"]
    get_names_starting_if_some_letter(friend_names)
    print(len(friend_names))
    # Exercise 2
    print_numbers_divisible_by_7(1, 30)
    # Exercise 3
    print(find_divisible_subset(1, 50, 9))
    # Exercise 4
    temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    print_temperatures_in_both_scales(temps_celsius)
    # Exercise 5
    print(print_temperatures_a_list_in_fahrenheit(temps_celsius))
